# Задача #
Есть две таблицы. Первая содержит основные данные по студентам:

~~~
#!mysql
CREATE TABLE student ( 
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    name VARCHAR(20) NOT NULL,
    surname VARCHAR(20) DEFAULT '' NOT NULL,
    gender ENUM('male', 'female', 'unknown') DEFAULT 'unknown',
    INDEX gender ( gender ) 
);
~~~

Вторая содержит историю статусов студентов, где последний по хронологии статус является текущим:

~~~
#!mysql
CREATE TABLE student_status ( 
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    student_id INT NOT NULL,
    status ENUM('new', 'studying', 'vacation', 'testing', 'lost') DEFAULT 'new' NOT NULL,
    datetime DATETIME NOT NULL,
    INDEX student_id ( student_id ),
    INDEX datetime ( datetime )
);
~~~

Необходимо показать имена и фамилии всех студентов, чей пол до сих не известен (gender = 'unknown') и они сейчас находятся на каникулах (status = ‘vacation’).

# Решение #

~~~
#!mysql
SELECT
  s.name,
  s.surname
FROM
  student AS s
INNER JOIN
  student_status AS ss
    ON
  ss.student_id = s.id
JOIN
  (
    SELECT
      id
    FROM
    (
      SELECT
        id,
        student_id
      FROM
        student_status
      ORDER BY
        datetime DESC
    )
    AS
      lastStatuses
    GROUP BY
      student_id
  )
  AS
    lastStatus
ON
  (lastStatus.id = ss.id)
WHERE
  ss.status = 'vacation' AND
  s.gender = 'unknown'
~~~

# Пояснение к решению #

1. Сначала во внутреннем SELECT достаём все статусы студентов и сортируем по дате по убыванию
2. Группируем внутренний SELECT по стобцу student_id и получаем массив студентов с последними статусами
3. Используем JOIN для подключения таблицы статусов 
4. Используем JOIN с внутренним селектом для выборки только последних статусов 
5. Достаем имя и фамилию студентов, используя условия данные в задаче(неопределенный пол и статус "на каникулах")

> В теории мы могли бы использовать внутренний селект для выборки последних статусов по id, что в свою очередь было бы более быстрее с точки зрения скорости выполнения, но id с автоинкрементом не гарантирует нам хронологическую достоверность, т.к. при использовании 1 мастер сервера базы данных и нескольких веб-серверов для балансировки нагрузки есть вероятность что один запрос на INSERT который был инициирован раньше по времени дойти до мастер сервера бд может позже
