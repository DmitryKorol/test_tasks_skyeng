<?php

/* @var $this yii\web\View */
/* @var $periods array */
/* @var $model \app\models\ConversionForm */

$this->title = 'My Yii Application';

use miloschuman\highcharts\Highcharts;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
?>

<div class="site-index">

    <?= Highcharts::widget([
        'options' => [
            'title' => ['text' => 'Конверсия '],
            'xAxis' => [
                'categories' => array_keys($periods)
            ],
            'yAxis' => [
                'title' => ['text' => '%']
            ],
            'series' => [
                [
                    'name' => 'Конверсия',
                    'data' => array_values($periods),
                    'tooltip' => [
                        'valueSuffix' => '%',
                    ],
                ],
            ]
        ]
    ]) ?>

    <?php $form = ActiveForm::begin() ?>

    <?= $form->field($model, 'period'); ?>

    <?= Html::submitButton('Установить', ['class' => 'btn btn-primary']) ?>

    <?php ActiveForm::end() ?>
</div>
