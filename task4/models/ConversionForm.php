<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий Король
 * Date: 07.02.2017
 * Time: 21:03
 */

namespace app\models;


use yii\base\Model;

class ConversionForm extends Model
{
    public $period = 10;

    public function rules()
    {
        return [
            ['period', 'required'],
            ['period', 'integer'],
        ];
    }

    public function attributeLabels()
    {
       return [
            'period' => 'Период',
       ];
    }

    public function attributeHints()
    {
        return [
            'period' => 'Длительность периода в днях',
        ];
    }
}
