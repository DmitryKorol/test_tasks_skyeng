<?php

use yii\db\Migration;
use app\models\User;


class m170206_175806_insert_users extends Migration
{
    private function getUsers()
    {
        return [
            [
                'firstName' => 'Test1',
                'lastName' => 'Test1',
                'phone' => 78990010023,
                'created_at' => strtotime("2016-02-16 18:43:55"),
            ],
            [
                'firstName' => 'Test2',
                'lastName' => 'Test2',
                'phone' => 78990010024,
                'created_at' => strtotime("2016-02-18 18:43:55"),
            ],
            [
                'firstName' => 'Test3',
                'lastName' => 'Test3',
                'phone' => 78990010025,
                'created_at' => strtotime("2016-02-18 20:43:55"),
            ],
            [
                'firstName' => 'Test4',
                'lastName' => 'Test4',
                'phone' => 78990010026,
                'created_at' => strtotime("2016-02-18 21:43:55"),
            ],
            [
                'firstName' => 'Test5',
                'lastName' => 'Test5',
                'phone' => 78990010027,
                'created_at' => strtotime("2016-03-16 18:43:55"),
            ],
            [
                'firstName' => 'Test6',
                'lastName' => 'Test6',
                'phone' => 78990010028,
                'created_at' => strtotime("2016-03-16 18:44:55"),
            ],
            [
                'firstName' => 'Test7',
                'lastName' => 'Test7',
                'phone' => 78990010029,
                'created_at' => strtotime("2016-03-16 18:47:55"),
            ],
            [
                'firstName' => 'Test8',
                'lastName' => 'Test8',
                'phone' => 78990010030,
                'created_at' => strtotime("2016-03-18 18:43:55"),
            ],
            [
                'firstName' => 'Test9',
                'lastName' => 'Test9',
                'phone' => 78990010031,
                'created_at' => strtotime("2016-03-22 10:43:55"),
            ],
            [
                'firstName' => 'Test10',
                'lastName' => 'Test10',
                'phone' => 78990010032,
                'created_at' => strtotime("2016-03-22 18:43:55"),
            ],
            [
                'firstName' => 'Test11',
                'lastName' => 'Test11',
                'phone' => 78990010033,
                'created_at' => strtotime("2016-03-23 18:43:55"),
            ],
        ];
    }
    public function up()
    {
        $users = $this->getUsers();

        foreach($users as $userParams) {
            $user = new User();

            foreach($userParams as $userParamName=>$userParamValue) {
                $user->$userParamName = $userParamValue;
            }

            $user->save();
        }
    }

    public function down()
    {
        return true;

        echo "m170206_175806_insert_users cannot be reverted.\n";

        return false;
    }
}
