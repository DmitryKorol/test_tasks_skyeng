<?php

namespace app\controllers;

use app\models\ConversionForm;
use app\models\User;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Display conversion page
     *
     * @return string
     */
    public function actionIndex()
    {
        $form = new ConversionForm();
        $form->load(\Yii::$app->request->getBodyParams());

        $totalUsersCount = User::find()->count();

        $firstUser = User::find()->orderBy(['created_at' => SORT_ASC])->limit(1)->all();
        $firstUserCreatedAt = $firstUser[0]->created_at;
        $firstUserCreatedDate = new \DateTime(date("Y-m-d", $firstUserCreatedAt));

        $periods = [];
        $periodNum = 1;
        $usersProceed = 0;
        while($usersProceed < $totalUsersCount) {
            $startFrom = $firstUserCreatedDate->getTimestamp() + ($periodNum-1)*60*60*24*$form->period;
            $endOn = $startFrom + 60*60*24*$form->period - 1;

            $usersCountInCurrentPeriod = User::find()->where('created_at>'.$startFrom)->andWhere('created_at<'.$endOn)->count();

            $periods[] = round($usersCountInCurrentPeriod / $totalUsersCount * 100, 2);

            $periodNum++;
            $usersProceed += $usersCountInCurrentPeriod;
        }

        return $this->render('index', [
            'model' => $form,
            'periods' => $periods,
        ]);
    }
}
