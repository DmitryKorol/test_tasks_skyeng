# Задача #

Используя три предыдущие таблицы, найти имена и фамилии всех студентов, которые заплатили не больше трех раз и перестали учиться (status = ‘lost’). Нулевые платежи (amount = 0) не учитывать.

# Решение #

~~~
#!mysql
SELECT
  s.name,
  s.surname
FROM
  student AS s
INNER JOIN
  student_status AS ss
    ON
  ss.student_id = s.id
JOIN
  (
    SELECT
      id
    FROM
    (
      SELECT
        id,
        student_id
      FROM
        student_status
      ORDER BY
        datetime DESC
    )
    AS
      lastStatuses
    GROUP BY
      student_id
  )
  AS
    lastStatus
ON
  (lastStatus.id = ss.id)
INNER JOIN
    (
      SELECT
        student_id
      FROM
        payments
      WHERE
        amount != 0
      GROUP BY
        student_id
      HAVING
        count(*) <= 3 AND
        count(*) > 0
    )
    AS
      p
    ON
      p.student_id = s.id
WHERE
  ss.status = 'lost'
~~~

# Пояснение к решению #

1. Используем решение из [задания 2](task2.md)
2. Подключаем таблицу платежей, где есть ненулевые платежи и их не больше 3
3. Достаём из общей массы полученных студентов по условию из задачи(студенты которые перестали учиться (status=lost))
