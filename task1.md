# Задача #
Есть таблица платежей пользователей:

~~~
#!mysql
CREATE TABLE payments ( 
    id INT AUTO_INCREMENT PRIMARY KEY NOT NULL, 
    student_id INT NOT NULL, 
    datetime DATETIME NOT NULL, 
    amount FLOAT DEFAULT 0, 
    INDEX student_id ( student_id ) 
);
~~~

Необходимо составить запрос, который находит пользователя, сумма платежей которого находится на втором месте после максимальной.

# Решение #

~~~
#!mysql
SELECT
  student_id,
  SUM(amount) AS amount_sum
FROM
  payments
GROUP BY
  student_id
ORDER BY
  SUM(amount) DESC
LIMIT
  1,1
~~~

# Пояснение к решению #

1. Достаём все записи по платежам
2. Группируем по столбцу student_id
3. Суммируем столбец amount всех сгруппированных записей и сортируем в порядке убывания
4. Возвращаем одну запись, которая имеет вторую позицию
